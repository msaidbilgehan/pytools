"""
    Refs:
        - init_logging
            -- https://realpython.com/python-logging
        -

"""

import os
import time
import random
import shutil
import logging
from glob import glob
from inspect import currentframe, getframeinfo
from stdo import stdo, get_time


time_struct = {"start": 0, "end": 0, "passed": 0}

time_list = {}


def get_file_name(
    name="file",
    extension=None,
    with_time=True,
    random_seed=None,
    random_min_max=[0, 1024],
):
    if with_time:
        if extension is not None:
            file_name = "{}_{}.{}".format(name, get_time(level=2), extension)
        else:
            file_name = "{}_{}".format(name, get_time(level=2))
    else:
        if random_seed is None:
            random.seed(time.time())
        else:
            random.seed(random_seed)
        if extension is not None:
            file_name = "{}_{}.{}".format(
                name, random.randint(random_min_max[0], random_min_max[1]), extension
            )
        else:
            file_name = "{}_{}".format(
                name, random.randint(random_min_max[0], random_min_max[1])
            )
    return file_name


def get_OS():
    OS = ""

    if os.name == "nt":
        OS = "W"  # Windows
    else:
        OS = "P"  # Posix (Linux etc...)

    return OS


def time_log(option, id="id"):
    global time_list, time_struct

    if len(time_list) == 0:
        time_list[id] = time_struct.copy()
    elif id != "id" and id not in time_list:
        time_list[id] = time_struct.copy()

    if option == "end" and time_list[id]["start"] == 0:
        stdo(2, "Start time is not initilized. Taken time will be saved as start time.")
        option = "start"

    time_list[id][option] = time.time()  # To count program start time
    if option == "end":
        time_list[id]["passed"] = (time_list[id]["end"] - time_list[id]["start"]) * 1000


def save_to_json(data, path):
    import json

    with open(path, "w") as outfile:
        json.dump(data, outfile)
    return 0


def load_from_json(path):
    import json

    data = None
    with open(path) as json_file:
        data = json.load(json_file)
    return data


def dict_to_list(dict, is_only_value):
    dict_list = list()
    for key, value in dict.iteritems():
        if is_only_value:
            dict_list.append(value)
        else:
            dict_list.append([key, value])
    return dict_list


def list_files(path="", name="*", extensions=["png"], recursive=False, verbose=True):
    # https://mkyong.com/python/python-how-to-list-all-files-in-a-directory/
    try:
        files = list()
        if recursive:
            if path[-1] != "/":
                path = path + "/"
        else:
            if path[-1] == "/":
                path = path[:-1]

        for extension in extensions:
            files.extend(
                [
                    f
                    for f in glob(
                        path + "**/{}.{}".format(name, extension), recursive=recursive
                    )
                ]
            )

        """ RECURSİVE
        for path in files:
            if path.split("/")[-2] != ""
        """
        if verbose:
            output = "- {}".format(path)
            for subPath in files:
                subPath = subPath.replace(path, "")
                output += "\n"
                for i in range(len(path.split("/"))):
                    output += "\t"
                output += "\t|- {}".format(subPath)
            stdo(1, output)
            stdo(1, "{} files found".format(len(files)))

        return files

    except Exception as error:
        stdo(
            3,
            "An error occured while working in fileList function -> " + error.__str__(),
            getframeinfo(currentframe()),
        )
        stdo(1, "--- --- ---")
    return None


def list_to_dict(datas):
    dictionary = dict()

    id = 1
    for data in datas:
        dictionary[id] = data
        id += 1
    return dictionary


def name_parsing(
    filePath, separate=False, separator=".", maxSplit=-1
):  # parsing name from file path
    file = filePath.split("/")[-1]
    extension = file.split(".")[-1]
    name = file.strip("." + extension)
    if separate:
        # https://www.programiz.com/python-programming/methods/string/strip
        name = name.split(separator, maxSplit)
    return name, extension


def clear_dir(dir_path, ignore_errors=False):
    stdo(1, string="'{}' path deleting...".format(dir_path))
    try:
        shutil.rmtree(dir_path, ignore_errors, onerror=None)
        stdo(1, string="'{}' path deleted".format(dir_path))

    except FileNotFoundError:
        stdo(1, string="'{}' path is not available".format(dir_path))

    except OSError as error:
        stdo(
            3,
            "Error Occured while working in 'clear_dir': {}".format(error.__str__()),
            getframeinfo(currentframe()),
        )
        exit(-1)
