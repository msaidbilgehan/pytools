# DISCONTINUED!
[New Project Link](https://gitlab.com/msaidbilgehan/lera-developer-framework)


# PYTOOLS

Written with Python

Tested with Python 3.5.3 and 3.7.7

# Project

## Software Details

### Global Variables

   - time_struct
   - time_list


### Functions

   - stdo()
   - get_time()
   - get_OS()
   - time_log()
   - list_files()
   - name_parsing()
   - clear_dir()


### Dependencies

Necessary Dependencies for built-in Python Libraries which are;
   - os
   - time
   - shutil
   - glob from glob
   - currentframe and getframeinfo from inspect
