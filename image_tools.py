from tools import get_time

# from tools import stdo
# from inspect import currentframe, getframeinfo
from PIL import Image
import cv2
import numpy as np
from matplotlib import pyplot as plt
import os  # For remove or check files
import errno  # https://stackoverflow.com/questions/36077266/how-do-i-raise-a-filenotfounderror-properly
import logging

# import time # Delay for camera module
import random

# from time import sleep


# https://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
def take_image(option):
    option = option.lower()
    if option == ("cam" or "pc"):
        """
        Not necessary but may help with PC applications
        """
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html

        cap = cv2.VideoCapture(0)
        # Capture frame-by-frame
        # Save the resulting frame to the shared area
        success, source_image = cap.read()
        if success:
            # When everything done, release the capture
            cap.release()
        else:
            logging.error("Camera connection failed.")
            return -1

    if option == "picam":

        # import pdb; pdb.set_trace() # Pi Camera DEBUG
        # For take pictures from Raspberry Pi Camera
        from picamera.array import PiRGBArray
        from picamera import PiCamera

        # initialize the camera and grab a reference to the raw camera capture
        # ISSUE: https://www.raspberrypi.org/forums/viewtopic.php?t=97188
        # camera = PiCamera()
        # rawCapture = PiRGBArray(camera)

        with PiCamera() as camera:
            # camera.capture('image.jpg')
            rawCapture = PiRGBArray(camera)

            # allow the camera to warmup
            # time.sleep(0.1)

            # grab an image from the camera
            camera.capture(rawCapture, format="bgr")
            source_image = rawCapture.array

    return source_image


def get_images(path):
    from os import listdir
    from os.path import isfile, join

    only_files = [f for f in listdir(path) if isfile(join(path, f))]

    temp_image_list = dict()
    i = 0
    for file_path in only_files:
        file_data = {
            "image": open_image(file_path, "cv2-grayscale"),
            "type": "<class 'numpy.ndarray'>",
            "name": "{}".format(str(file_path)),
            "path": "{}".format(path),
        }

        temp_image_list[i, file_data]
        i += 1

    return temp_image_list


def save_image(img, path=""):
    try:
        if path == "":  # If path is empty, then we refer to temp file save protocol
            file_name = get_temp_name()
            path = "temp_files/{}".format(file_name)

        file_name = path.split("/")[-1]
        dir_list = path.split("/")[:-1]

        if dir_list != []:
            directory = ""
            for dir in dir_list:
                directory += dir + "/"

            if not os.path.exists(directory):
                os.makedirs(directory, exist_ok=True, mode=0o777)

            if not os.path.exists(directory):
                raise IOError
            else:
                logging.info(
                    "'{}' Directory directory created.".format(directory),
                )  # file_name = str(path.split('/')[-1])
                # cv2.imwrite(path, img)

        logging.info("Image will be saved as '{}'".format(file_name))
        cv2.imwrite(path, img)
        return path

    except IOError as error:
        logging.exception(
            "Directory creation failed, please raport this issue to developer -> {}".format(
                error.__str__()
            )
        )
        return -1

    except Exception as error:
        logging.exception(
            "An error occured while save_image function runtime -> " + error.__str__()
        )
        return -1
    return 0


def delete_image(path):
    try:
        if os.path.exists(path):
            os.remove(path)
        else:
            raise Exception(errno.ENOENT, os.strerror(errno.ENOENT), path)
    except Exception as error:
        logging.exception(
            "An error occured while delete_image function runtime -> " + error.__str__()
        )
        return -1
    return 0


def open_image(img_location, option="cv2-grayscale", is_numpy=False):
    output = "'{}' Image opened with '{}' option.".format(
        img_location.split("/")[-1], option
    )

    try:
        if os.path.exists(img_location):
            source_image = None

            if option == "cv2-rgb":
                source_image = cv2.imread(
                    img_location, cv2.IMREAD_COLOR
                )  # Read image as colorfull (RGB)

            elif option == "cv2-grayscale":
                source_image = cv2.imread(
                    img_location, cv2.IMREAD_GRAYSCALE
                )  # Read image as grayscale

            elif option == "PIL":
                source_image = Image.open(img_location)

            elif option == "skimage":
                import skimage as ski

                source_image = ski.io.imread(img_location)

            else:
                logging.error("Option Not Found! -> " + option)
                logging.info("Program terminated immediately.")
                return -1
        else:
            raise FileNotFoundError(
                errno.ENOENT, os.strerror(errno.ENOENT), img_location
            )

        if is_numpy is True:
            source_image = np.array(cv2.cvtColor(source_image, cv2.COLOR_BGR2RGB))
        logging.info(output)
        return source_image

    except IOError as ioe:
        logging.exception("Image Not Found! -> " + ioe.__str__())
        return -1

    except FileNotFoundError as fnfe:
        logging.exception("File Not Found! -> " + fnfe.__str__())
        return -1

    except Exception as error:
        logging.exception(
            "An error occured while working in open_image function -> "
            + error.__str__()
        )
        logging.warning("Program terminated immediately.")
        return -1


def open_temp_image(img_location, option, no_cache=True):
    output = "'{}' Temprorary image opened with '{}' option.".format(
        img_location.split("/")[-1], option
    )

    try:
        if no_cache:
            if option == "cv2-rgb":
                return cv2.imread(
                    img_location, cv2.IMREAD_COLOR
                )  # Read image as colorfull (RGB)

            elif option == "cv2-grayscale":
                return cv2.imread(
                    img_location, cv2.IMREAD_GRAYSCALE
                )  # Read image as grayscale

            elif option == "PIL":
                return Image.open(img_location)

            elif option == "skimage":
                import skimage as ski

                return ski.io.imread(img_location)

            else:
                logging.error("Option Not Found! -> " + option)
                logging.warning("Image open process skipped.")
                return -1

            logging.info(output)
            return 0
        else:
            """
            This part is still at development.
            Aim is store image data in ram with using shared area.
            """
            pass

    except IOError as ioe:
        logging.exception("Image Not Found! -> " + ioe.__str__())
        logging.info("Image open process skipped.")
        return -1

    except FileNotFoundError as fnfe:
        logging.exception("File Not Found! -> " + fnfe.__str__())
        logging.info("Image open process skipped.")
        return -1

    except Exception as error:
        logging.exception(
            "An error occured while working in open_image function -> "
            + error.__str__()
        )
        logging.info("Image open process skipped.")
        return -1


def show_image(
    source,
    title="Title",
    option="plot",
    cmap="gray",
    open_order=None,
    window=False,
    event_click=False,
):
    logging.info("Image will be shown now with title '{}'.".format(title))

    if window:
        if isinstance(source, (list, tuple)):
            length_of_source_list = len(source)
            if open_order is None:
                for i in range(0, length_of_source_list):
                    show_image(
                        source[i], title=title, option=option, cmap=cmap, window=window
                    )
            elif open_order > 0:
                # https://stackoverflow.com/questions/46615554/how-to-display-multiple-images-in-one-figure-correctly/46616645
                import matplotlib.pyplot as plot

                figure = plot.figure()

                columns = open_order
                if open_order >= length_of_source_list:
                    rows = 1
                else:
                    if (length_of_source_list % open_order) > 0:
                        rows = int(length_of_source_list / open_order) + 1
                    else:
                        rows = int(length_of_source_list / open_order)

                for i in range(1, length_of_source_list + 1):
                    figure.add_subplot(rows, columns, i)
                    plot.imshow(source[i - 1], cmap=cmap)

                click_coordinates = []
                if event_click:

                    def onclick(event, is_verbose=True):
                        nonlocal click_coordinates

                        if event.dblclick:
                            ix, iy = event.xdata, event.ydata

                            if is_verbose:
                                print("Choosen: x = %d, y = %d" % (ix, iy))

                            click_coordinates.append((round(ix), round(iy)))

                    figure.canvas.mpl_connect("button_press_event", onclick)

                plot.show()
                return click_coordinates
        else:
            if option == "cv2":
                try:
                    cv2.namedWindow(title, cv2.WINDOW_NORMAL)
                    cv2.resizeWindow(title, 600, 600)

                    cv2.imshow(title, source)
                    cv2.waitKey()
                    cv2.destroyAllWindows()

                except Exception as error:
                    logging.error(
                        "An error occured while working in show_image function -> "
                        + error.__str__()
                    )
            elif option == "plot":
                try:
                    """
                    w = 10
                    h = 10

                    #fig = plt.figure(figsize=(8, 8))

                    columns = 1
                    rows = 1

                    for i in range(1, columns * rows + 1):
                        fig.add_subplot(rows, columns, i)
                        plt.imshow(image, cmap=cmap)
                    """
                    # fig = plt.figure()
                    # fig.add_subplot(1, 1, 1)

                    plt.imshow(source, cmap=cmap)
                    plt.show()

                except Exception as error:
                    logging.error(
                        "An error occured while working in show_image function -> "
                        + error.__str__()
                    )
            else:
                logging.warning("Invalid option")

        """ # For skimage
        plt.imshow(image, cmap='gray', interpolation='nearest')
        plt.show()
        """
    return 0


def rgb2gray(img, verbose=False):
    try:
        if len(img.shape) > 2:
            if img.shape[-1] != 1:
                if verbose:
                    logging.info("RGB Image converted to grayscale.")
                return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            else:
                return img
        else:
            if verbose:
                logging.warning("Image is already grayscale. Returning given image...")
            return img
    except Exception as error:
        logging.exception(
            "An error occured while working in rgb2gray function -> " + error.__str__()
        )
        return -1


def compare2images(img, img2, title="Title"):
    logging.info("Images will be shown now with title '{}'.".format(title))
    cv2.namedWindow(title, cv2.WINDOW_NORMAL)

    try:
        cv2.imshow(title, np.hstack([img, img2]))
        cv2.waitKey()
        cv2.destroyAllWindows()
    except Exception as error:
        logging.exception(
            "An error occured while working in compare2images function -> "
            + error.__str__(),
        )
    return 0


def info_image(img):
    # option for skimage and numpy image desicion
    try:
        # https://www.programiz.com/python-programming/methods/built-in/isinstance
        output = """Image
                    |- Width: {0}
                    |- Height: {1}
                    |- Size: {2}
                    '- Type/Class: {3}"""
        logging.info(
            output.format(
                str(img.shape[0]), str(img.shape[1]), str(img.size), str(type(img))
            ),
        )
    except Exception as error:
        logging.exception(
            "An error occured while working in info_image function -> "
            + error.__str__(),
        )
    return 0


def get_temp_name(seed=None, extension="png"):
    if seed is None:
        seed = get_time(level=0)
    random.seed(seed * 1000000)

    name = get_time(2)

    if name[-1] == "_":
        name = "{}0_{}".format(name[:-1], random.randint(1000, 9999))
    else:
        name = "{}_{}".format(name, random.randint(1000, 9999))

    return "temp_image-{}.{}".format(name, extension)


def info_video(video, option=""):
    # option for skimage and numpy image desicion

    try:
        # https://www.programiz.com/python-programming/methods/built-in/isinstance
        output = """Video
                    |- Width: {0}
                    |- Height: {1}
                    |- FPS: {2}
                    |- Size: {3}
                    '- Type/Class: {4}"""
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))  # always 0 in Linux python3
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))  # always 0 in Linux python3
        fps = video.get(cv2.CAP_PROP_FPS)
        logging.info(
            output.format(
                str(width),
                str(height),
                str(fps),
                str(width * height),
                str(type(video)),
            ),
        )
    except Exception as error:
        logging.exception(
            "An error occured while working in info_video function -> "
            + error.__str__(),
        )
    return 0
